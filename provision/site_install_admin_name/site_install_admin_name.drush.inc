<?php

/**
* Implementation of hook_post_provision_install().
*/
function drush_site_install_admin_name_post_provision_install() {
  $account = user_load(1);
  $edit = array();
  $edit['name'] = drush_get_option('admin_username', 'admin');
  drush_log(dt('Updating the Drupal super user to be named: @name', array('@name' => $edit['name'])));
  user_save($account,  $edit);
}