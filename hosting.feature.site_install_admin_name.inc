<?php

/**
 * Implementation of hook_hosting_feature().
 */
function hosting_site_install_admin_name_hosting_feature() {
  $features = array();
  $features['site_install_admin_name'] = array(
    'title' => t("Hosting site install admin name"),
    'description' => t("Allows site administrators to change the name of the uid1 user when installing a site using aegir"),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_site_install_admin_name',
    'group' => 'experimental',
  );
  return $features;
}
