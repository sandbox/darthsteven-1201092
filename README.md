Hosting site install admin name
===============================

Allows site administrators to change the name of the uid1 user when installing a
site using aegir.

Installation
------------

Install as a normal Drupal module, but move the contents of the provision folder
to your provision folder (usually ~/.drush/provision).

Configuration
-------------

Change the username used to create sites at `admin/hosting/settings`.

Limitations
-----------

This will change the username after Aegir has generated the welcome email, so if
the username appears in that email it will be wrong.