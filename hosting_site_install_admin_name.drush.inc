<?php

/**
 * Implementation of drush_hook_pre_hosting_task().
 */
function drush_hosting_site_install_admin_name_pre_hosting_task() {
  $task =& drush_get_context('HOSTING_TASK');
  if (($task->ref->type == 'site') && ($task->task_type == 'install')) {
    $task->options['admin_username'] = variable_get('hosting_site_install_admin_name', 'admin');
  }
}